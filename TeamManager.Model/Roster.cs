﻿using System.Collections.Generic;

namespace TeamManager.Model
{
    public class Roster : BaseEntity
    {
        public virtual List<Player> Players { get; set; }
    }
}
