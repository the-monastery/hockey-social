﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TeamManager.Model
{
    public abstract class BaseEntity
    {
        [Key]
        public int Id { get; set; }

        public Guid UniqueId { get; set; }

        public string DisplayName { get; set; }

        public DateTime CreationDate { get; set; }

        public DateTime LastUpdate { get; set; }

        protected BaseEntity()
        {
            UniqueId = Guid.NewGuid();
            CreationDate = DateTime.UtcNow;
            LastUpdate = DateTime.UtcNow;
        }
    }
}
