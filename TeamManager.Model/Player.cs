﻿using System;
using System.Collections.Generic;
using TeamManager.Model.enums;

namespace TeamManager.Model
{
    public class Player : BaseEntity
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime? BirthDate { get; set; }

        public Gender Gender { get; set; }

        public virtual List<HockeySkill> HockeySkills { get; set; }

        public Handedness Handedness { get; set; }

        public virtual Address Address { get; set; }

        public virtual Contact Contact { get; set; }
    }
}
