﻿namespace TeamManager.Model
{
    public enum ExperienceLevel
    {
        E, D, C, B, A, CC, BB, AA, CCC, BBB, AAA, SemiPro, Pro
    }
}