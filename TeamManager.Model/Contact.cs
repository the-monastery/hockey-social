﻿using System;

namespace TeamManager.Model
{
    public class Contact : BaseEntity
    {
        public string Email { get; set; }

        public string PhoneNumber1 { get; set; }

        public string PhoneNumber2 { get; set; }
    }
}