﻿namespace TeamManager.Model
{
    public class Team : BaseEntity
    {
        public string Color { get; set; }

        public virtual Roster CurrentRoster { get; set; }
    }
}
