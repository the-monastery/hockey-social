﻿namespace TeamManager.Model.enums
{
    public enum Position
    {
        Goalie, 
        RightWing, 
        LeftWing, 
        Center, 
        LeftDefense, 
        RightDefense
    }
}