﻿namespace TeamManager.Model.enums
{
    public enum Handedness
    {
        Left,
        Right
    }
}
