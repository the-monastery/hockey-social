﻿namespace TeamManager.Model.enums
{
    public enum Level
    {
        Atom,
        PeeWee,
        Bantam,
        Midget,
        Junior,
        SemiPro,
        Pro,
    }
}
