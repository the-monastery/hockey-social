﻿namespace TeamManager.Model.enums
{
    public enum Category
    {
        E, D, C, B, A, CC, BB, AA, CCC, BBB, AAA, SemiPro, Pro
    }
}