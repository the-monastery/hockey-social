﻿using System.Collections.Generic;

namespace TeamManager.Model
{
    public class Organizer : Player
    {
        public virtual List<League> Leagues { get; set; }
    }
}
