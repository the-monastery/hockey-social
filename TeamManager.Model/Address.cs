﻿using System;

namespace TeamManager.Model
{
    public class Address : BaseEntity
    {
        public string Line1 { get; set; }

        public string Line2 { get; set; }

        public string Code { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

        public double? Latitude { get; set; }

        public double? Longitude { get; set; }
    }
}
