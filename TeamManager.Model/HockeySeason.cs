﻿using System;

namespace TeamManager.Model
{
    public class HockeySeason : BaseEntity
    {
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }
    }
}
