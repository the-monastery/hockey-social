﻿using System.Collections.Generic;

namespace TeamManager.Model
{
    public class League : BaseEntity
    {
        public virtual List<Team> Teams { get; set; }

        public virtual HockeySeason LastSeason { get; set; }

        public virtual List<HockeySeason> PastSeasons { get; set; }
    }
}
