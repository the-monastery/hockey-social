﻿using System;

namespace TeamManager.Model
{
    public class Game : BaseEntity
    {
        public virtual Arena Arena { get; set; }

        public DateTime DateTime { get; set; }

        public TimeSpan Duration { get; set; }

        public virtual Team HomeTeam { get; set; }

        public virtual Team VisitingTeam { get; set; }
    }
}
