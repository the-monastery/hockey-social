﻿namespace TeamManager.Model
{
    public enum Position
    {
        Goalie, 
        RightWing, 
        LeftWing, 
        Center, 
        LeftDefense, 
        RightDefense
    }
}