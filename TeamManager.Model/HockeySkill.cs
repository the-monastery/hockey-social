﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using TeamManager.Model.enums;

namespace TeamManager.Model
{
    public class HockeySkill : BaseEntity
    {
        public Category HighestCategoryPlayed { get; set; }

        public Level HighestLevelPlayed { get; set; }

        public Position Position { get; set; }
    }
}
