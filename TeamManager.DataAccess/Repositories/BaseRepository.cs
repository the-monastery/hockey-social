﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace TeamManager.DataAccess.Repositories
{
    public class BaseRepository<T> : IRepository<T> where T : class
    {
        public virtual T Get(DbContext context, int id)
        {
            return context.Set<T>().Find(id);
        }

        public virtual IQueryable<T> GetWhere(DbContext context, Expression<Func<T, bool>> expression)
        {
            IQueryable<T> result = context.Set<T>().AsQueryable();
            return (expression == null) ? result : result.Where<T>(expression);
        }

        public virtual void Add(DbContext context, T entity)
        {
            context.Entry(entity).State = EntityState.Added;
        }

        public virtual void Update(DbContext context, T entity)
        {
            context.Entry(entity).State = EntityState.Modified;
        }

        public virtual void Delete(DbContext context, T entity)
        {
            context.Entry(entity).State = EntityState.Deleted;
        }

        public virtual int Save(DbContext context)
        {
            return context.SaveChanges();
        }
    }
}
