﻿using System.Data.Entity;
using TeamManager.Model;

namespace TeamManager.DataAccess.Repositories
{
    public class PlayerRepository: BaseRepository<Player>
    {
        public override void Update(DbContext context, Player entity)
        {
            context.Entry(entity).State = EntityState.Modified;
            context.Entry(entity.Address).State = EntityState.Modified;
            context.Entry(entity.Contact).State = EntityState.Modified;
        }
    }
}
