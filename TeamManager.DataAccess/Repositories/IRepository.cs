﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace TeamManager.DataAccess.Repositories
{
    public interface IRepository<T> where T : class
    {
        /// <summary>
        ///     Gets an entity of type T using its Id
        /// </summary>
        /// <param name="context"> </param>
        /// <param name="id">Entity Id as int</param>
        /// <returns>The requested entity of type T or null</returns>
        T Get(DbContext context, int id);

        /// <summary>
        ///     Gets many entities according to a Lambda query expression
        /// </summary>
        /// <param name="context"> </param>
        /// <param name="expression">The expression tree for the query</param>
        /// <returns>a List(T) of entities</returns>
        IQueryable<T> GetWhere(DbContext context, Expression<Func<T, bool>> expression);

        /// <summary>
        ///     Saves a new entity to the Database/Datastore
        /// </summary>
        /// <param name="context"> </param>
        /// <param name="entity">The entity to persist</param>
        /// <returns>True if successfully inserted into the DB</returns>
        void Add(DbContext context, T entity);

        /// <summary>
        ///     Update an existing entity in the Database/Datastore
        /// </summary>
        /// <param name="context"> </param>
        /// <param name="entity">The entity to update</param>
        /// <returns>True if no exeption is thrown</returns>
        void Update(DbContext context, T entity);

        /// <summary>
        ///     Delete an existing entity in the Database/Datastore
        /// </summary>
        /// <param name="context"> </param>
        /// <param name="entity">The entity to delete</param>
        /// <returns>True if no exeption is thrown</returns>
        void Delete(DbContext context, T entity);

        /// <summary>
        /// Saves changes for dbcontext
        /// </summary>
        /// <returns></returns>
        int Save(DbContext context);
    }
}
