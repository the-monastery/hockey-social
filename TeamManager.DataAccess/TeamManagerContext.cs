﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using TeamManager.Model;

namespace TeamManager.DataAccess
{
    public class TeamManagerContext : IdentityDbContext
    {
        //database is dropped and recreated if model changes. this can be changed by using another initializer
        public TeamManagerContext()
            : base("TeamManagerContext")
        {
            Database.SetInitializer(new TeamManagerDbInitializer());
            Database.Initialize(false);
        }

        public DbSet<Address> Addresses { get; set; }
        public DbSet<Arena> Arenas { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<Game> Games { get; set; }
        public DbSet<HockeySeason> HockeySeasons { get; set; }
        public DbSet<HockeySkill> HockeySkills { get; set; }
        public DbSet<League> Leagues { get; set; }
        public DbSet<Organizer> Organizers { get; set; }
        public DbSet<Player> Players { get; set; }
        public DbSet<Roster> Rosters { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<ApplicationUser> IdentityUsers { get; set; }

        public static TeamManagerContext Create()
        {
            return new TeamManagerContext();
        }
    }
}
