﻿using System.Data.Entity;

namespace TeamManager.DataAccess
{
    public class TeamManagerDbInitializer : DropCreateDatabaseAlways<TeamManagerContext>
    {
        protected override void Seed(TeamManagerContext context)
        {
            for (int i = 0; i < 5; i++)
            {
                context.Organizers.Add(DataGenerator.DataGenerator.CreateOrganizer());
            }

            for (int i = 0; i < 10; i++)
            {
                context.Games.Add(DataGenerator.DataGenerator.CreateGame());
            }
            base.Seed(context);
        }
    }
}
