﻿using System;
using System.Globalization;

namespace TeamManager.Utilities
{
    public static class DateTimeHelper
    {
        public static DateTime? GetDateTime(string dateTime, CultureInfo cultureInfo)
        {
            DateTime parsedDateTime;

            try
            {
                if (DateTime.TryParse(dateTime, cultureInfo, System.Globalization.DateTimeStyles.None, out parsedDateTime))
                {
                    return parsedDateTime;
                }

                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
