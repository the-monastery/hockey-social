﻿using System;

namespace TeamManager.Utilities
{
    public static class EnumHelper
    {
        public static TEnum Parse<TEnum>(string enumValue) where TEnum : struct
        {
            TEnum defaultReturnValue = default(TEnum);
            TEnum enumResult;

            try
            {
                if (Enum.TryParse<TEnum>(enumValue, true, out enumResult))
                {
                    return enumResult;
                }

                return defaultReturnValue;
            }
            catch (Exception ex)
            {
                return defaultReturnValue;
            }
        }
    }
}
