﻿using System;
using System.Globalization;

namespace TeamManager.Utilities
{
    public static class CultureHelper
    {
        public static CultureInfo GetCulture(string locale)
        {
            CultureInfo defaultCulture = new CultureInfo("en-US", true);

            if (string.IsNullOrEmpty(locale))
            {
                return defaultCulture;
            }

            try
            {
                CultureInfo culture = CultureInfo.GetCultureInfo(locale);

                return culture ?? defaultCulture;
            }
            catch (Exception ex)
            {
                return defaultCulture;
            }
        }
    }
}
