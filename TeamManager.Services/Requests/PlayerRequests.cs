﻿using TeamManager.Model;

namespace TeamManager.Services.Requests
{
    public class GetPlayerRequest
    {
        public int Id;
    }

    public class AddPlayerRequest
    {
        public Player Player;
    }

    public class DeletePlayerRequest
    {
        public int Id;

        public Player Player;
    }

    public class UpdatePlayerRequest
    {
        public Player Player;
    }
}
