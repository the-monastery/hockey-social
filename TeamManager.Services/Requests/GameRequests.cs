﻿using TeamManager.Model;

namespace TeamManager.Services.Requests
{
    public class GetGameRequest
    {
        public int Id;
    }

    public class AddGameRequest
    {
        public Game Game;
    }

    public class DeleteGameRequest
    {
        public int Id;

        public Game Game;
    }

    public class UpdateGameRequest
    {
        public Game Game;
    }
}
