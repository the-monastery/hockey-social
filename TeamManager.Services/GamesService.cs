﻿using System.Collections.Generic;
using System.Linq;
using TeamManager.DataAccess;
using TeamManager.DataAccess.Repositories;
using TeamManager.Model;
using TeamManager.Services.Requests;

namespace TeamManager.Services
{
    public class GamesService : BaseService, IGamesService
    {
        public GameRepository Repository { get; set; }

        public List<Game> GetAll(TeamManagerContext context)
        {
            return Repository.GetWhere(context, null).ToList();
        }

        public Game Get(TeamManagerContext context, GetGameRequest request)
        {
            return Repository.Get(context, request.Id);
        }

        public void AddGame(TeamManagerContext context, AddGameRequest request)
        {
            Repository.Add(context, request.Game);
            Repository.Save(context);
        }

        public void Delete(TeamManagerContext context, DeleteGameRequest request)
        {
            Repository.Delete(context, request.Game);
            Repository.Save(context);
        }

        public void Update(TeamManagerContext context, UpdateGameRequest request)
        {
            Repository.Update(context, request.Game);
            Repository.Save(context);
        }
    }
}
