using System.Collections.Generic;
using TeamManager.DataAccess;
using TeamManager.DataAccess.Repositories;
using TeamManager.Model;
using TeamManager.Services.Requests;

namespace TeamManager.Services
{
    public interface IGamesService
    {
        GameRepository Repository { get; set; }
        List<Game> GetAll(TeamManagerContext context);
        Game Get(TeamManagerContext context, GetGameRequest request);
        void AddGame(TeamManagerContext context, AddGameRequest request);
        void Delete(TeamManagerContext context, DeleteGameRequest request);
        void Update(TeamManagerContext context, UpdateGameRequest request);
    }
}