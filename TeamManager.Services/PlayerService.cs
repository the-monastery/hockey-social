﻿using System.Collections.Generic;
using System.Linq;
using TeamManager.DataAccess;
using TeamManager.DataAccess.Repositories;
using TeamManager.Model;
using TeamManager.Services.Requests;

namespace TeamManager.Services
{
    public class PlayerService : BaseService, IPlayerService
    {
        public PlayerRepository Repository { get; set; }

        public List<Player> GetAll(TeamManagerContext context)
        {
            return Repository.GetWhere(context, null).ToList();
        }

        public Player Get(TeamManagerContext context, GetPlayerRequest request)
        {
            return Repository.Get(context, request.Id);
        }

        public void AddPlayer(TeamManagerContext context, AddPlayerRequest request)
        {
            Repository.Add(context, request.Player);
            Repository.Save(context);
        }

        public void Delete(TeamManagerContext context, DeletePlayerRequest request)
        {
            Repository.Delete(context, request.Player);
            Repository.Save(context);
        }

        public void Update(TeamManagerContext context, UpdatePlayerRequest request)
        {
            Repository.Update(context, request.Player);
            Repository.Save(context);
        }
    }
}
