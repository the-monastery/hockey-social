﻿using System;

namespace TeamManager.Services.Geocoding
{
    public interface IGeocodeService
    {
        Tuple<double, double> GetLatitudeLongitude(string address);
    }
}
