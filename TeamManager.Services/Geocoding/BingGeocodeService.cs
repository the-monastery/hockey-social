﻿using System;
using System.IO;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using TeamManager.Model.BingMapsDataContract;

namespace TeamManager.Services.Geocoding
{
    public class BingGeocodeService : IGeocodeService
    {
        public Tuple<double, double> GetLatitudeLongitude(string address)
        {
            //synchronous call
            Response response = GetBingServiceResponse(address).Result;

            if(!IsResponseValid(response))
            {
                return null;
            }

            Location addressLocation = response.ResourceSets[0].Resources[0] as Location;

            if (addressLocation == null)
            {
                return null;
            }

            double longitude = addressLocation.GeocodePoints[0].Coordinates[0];
            double latitude = addressLocation.GeocodePoints[0].Coordinates[1];

            return new Tuple<double, double>(longitude, latitude);
        }

        private bool IsResponseValid(Response response)
        {
            if (response.StatusCode != (int)HttpStatusCode.OK)
            {
                return false;
            }

            return response.ResourceSets != null &&
                   response.ResourceSets.Length > 0 &&
                   response.ResourceSets[0] != null &&
                   response.ResourceSets[0].Resources.Length > 0;
        }

        private async Task<Response> GetBingServiceResponse(string address)
        {
            string key = "Ar_EgqKXNJjszNuTgxVCkN67quqpOYXnS09H56b8x52koP5q5bg_EyTKlRRTriHS ";
            string query = address;

            Uri geocodeRequest = new Uri(string.Format("http://dev.virtualearth.net/REST/v1/Locations?q={0}&maxResults=1&key={1}", query, key));

            return await GetResponse(geocodeRequest);
        }

        private async Task<Response> GetResponse(Uri uri)
        {
            WebClient wc = new WebClient();
            string jsonResult = await wc.DownloadStringTaskAsync(uri);

            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(Response));

            using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(jsonResult)))
            {
                return ser.ReadObject(ms) as Response;
            }
        }
    }
}
