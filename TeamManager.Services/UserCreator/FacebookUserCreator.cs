﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using TeamManager.Model;
using TeamManager.Model.enums;
using TeamManager.Utilities;

namespace TeamManager.Services.UserCreator
{
    public class FacebookUserCreator : IUserCreatorService
    {
        public ApplicationUser CreateUser(IEnumerable<Claim> claims, string email)
        {
            string fbFirstName = claims.First(c => c.Type == "urn:facebook:first_name").Value;
            string fbLastName = claims.First(c => c.Type == "urn:facebook:last_name").Value;
            string fbGender = claims.First(c => c.Type == "urn:facebook:gender").Value;
            string fbBirthday = claims.First(c => c.Type == "urn:facebook:birthday").Value;
            string fbLocale = claims.First(c => c.Type == "urn:facebook:locale").Value;

            CultureInfo cultureInfo = CultureHelper.GetCulture(fbLocale);
            DateTime? birthday = DateTimeHelper.GetDateTime(fbBirthday, cultureInfo);
            Gender gender = EnumHelper.Parse<Gender>(fbGender);

            Contact contact = new Contact
            {
                Email = email,
            };

            Player player = new Player
            {
                FirstName = string.IsNullOrEmpty(fbFirstName) ? string.Empty : fbFirstName,
                LastName = string.IsNullOrEmpty(fbLastName) ? string.Empty : fbLastName,
                Gender = gender,
                BirthDate = birthday,
                Contact = contact,
                Address = new Address()
            };

            ApplicationUser newUser = new ApplicationUser
            {
                UserName = email,
                Email = email,
                Player = player,
            };

            return newUser;

        }
    }
}
