﻿using System.Collections.Generic;
using System.Security.Claims;
using TeamManager.Model;
using TeamManager.Services.Geocoding;

namespace TeamManager.Services.UserCreator
{
    public interface IUserCreatorService
    {
        ApplicationUser CreateUser(IEnumerable<Claim> claims, string email);
    }
}
