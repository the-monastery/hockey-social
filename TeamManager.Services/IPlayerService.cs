using System.Collections.Generic;
using TeamManager.DataAccess;
using TeamManager.DataAccess.Repositories;
using TeamManager.Model;
using TeamManager.Services.Requests;

namespace TeamManager.Services
{
    public interface IPlayerService
    {
        PlayerRepository Repository { get; set; }
        List<Player> GetAll(TeamManagerContext context);
        Player Get(TeamManagerContext context, GetPlayerRequest request);
        void AddPlayer(TeamManagerContext context, AddPlayerRequest request);
        void Delete(TeamManagerContext context, DeletePlayerRequest request);
        void Update(TeamManagerContext context, UpdatePlayerRequest request);
    }
}