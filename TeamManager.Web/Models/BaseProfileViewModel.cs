﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeamManager.Model;

namespace TeamManager.Web.Models
{
    public class BaseProfileViewModel
    {   
        public ApplicationUser User { get; set; }
    }
}
