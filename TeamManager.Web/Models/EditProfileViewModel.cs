﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace TeamManager.Web.Models
{
    public class EditProfileViewModel : BaseProfileViewModel
    {
        public List<SelectListItem> GendreDropDown = new List<SelectListItem>
        {
            new SelectListItem{Text="Male",Value="0"},
            new SelectListItem{Text="Female", Value="1"}
        };

        public List<SelectListItem> HandednessDropDown = new List<SelectListItem>
        {
            new SelectListItem{Text="Left",Value="0"},
            new SelectListItem{Text="Right", Value="1"}
        };
    }
}