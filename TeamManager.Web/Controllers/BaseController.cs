﻿using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using TeamManager.DataAccess;
using TeamManager.Model;

namespace TeamManager.Web.Controllers
{
    public class BaseController : Controller
    {
        protected readonly TeamManagerContext Db = new TeamManagerContext();

        protected static ApplicationUser LoggedInUser { get; set; }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string userId = User.Identity.GetUserId();

            if (!string.IsNullOrEmpty(userId))
            {
                LoggedInUser = Db.IdentityUsers.Find(userId);
            }

            base.OnActionExecuting(filterContext);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                Db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}