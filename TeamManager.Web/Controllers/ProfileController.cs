﻿using System.Net;
using System.Web.Mvc;
using TeamManager.Model;
using TeamManager.Proxies;
using TeamManager.Web.Models;

namespace TeamManager.Web.Controllers
{
    public class ProfileController : BaseController
    {
        public PlayerProxy PlayerProxy { get; set; }

        // GET: Profile
        public ActionResult Index()
        {
            if (LoggedInUser != null)
            {
                return View("Index", LoggedInUser);
            }

            return View();
        }


        // GET: Profile/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ApplicationUser applicationUser = LoggedInUser;

            if (applicationUser == null)
            {
                return HttpNotFound();
            }

            EditProfileViewModel viewModel = ViewUtils.CreateEditProfileViewModel(applicationUser);

            return View(viewModel);
        }

        // POST: Profile/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EditProfileViewModel applicationUser)
        {
            if (ModelState.IsValid)
            {

                PlayerProxy.UpdatePlayer(Db, applicationUser.User.Player);

                return RedirectToAction("Index");
            }

            return View(applicationUser);
        }
    }
}
