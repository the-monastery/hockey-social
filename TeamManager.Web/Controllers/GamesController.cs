﻿using System.Web.Mvc;
using TeamManager.Model;
using TeamManager.Proxies;

namespace TeamManager.Web.Controllers
{
    public class GamesController : BaseController
    {
        public GameProxy GameProxy { get; set; }

        // GET: Games
        public ActionResult Index()
        {
            return View(GameProxy.GetAll(Db));
        }

        // GET: Games/Details/5
        public ActionResult Details(int id)
        {
            Game game = GameProxy.GetById(Db, id);

            if (game == null)
            {
                return HttpNotFound();
            }
            return View(game);
        }

        // GET: Games/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Games/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Game game)
        {
            if (ModelState.IsValid)
            {
                GameProxy.AddGame(Db, game);

                return RedirectToAction("Index");
            }

            return View(game);
        }

        // GET: Games/Edit/5
        public ActionResult Edit(int id)
        {
            Game game = GameProxy.GetById(Db, id);

            if (game == null)
            {
                return HttpNotFound();
            }

            return View(game);
        }

        // POST: Games/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Game game)
        {
            if (ModelState.IsValid)
            {
                GameProxy.UpdateGame(Db, game);

                return RedirectToAction("Index");
            }
            return View(game);
        }

        // GET: Games/Delete/5
        public ActionResult Delete(int id)
        {
            Game game = GameProxy.GetById(Db, id);

            if (game == null)
            {
                return HttpNotFound();
            }
            return View(game);
        }

        // POST: Games/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            GameProxy.RemoveGame(Db, id);

            return RedirectToAction("Index");
        }
    }
}
