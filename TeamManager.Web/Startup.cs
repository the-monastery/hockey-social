﻿using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using Microsoft.Owin;
using Owin;
using TeamManager.DataAccess;
using TeamManager.DataAccess.Repositories;
using TeamManager.Proxies;
using TeamManager.Services;
using TeamManager.Services.Geocoding;
using TeamManager.Services.UserCreator;
using TeamManager.Web;

[assembly: OwinStartup(typeof(Startup))]
namespace TeamManager.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var builder = new ContainerBuilder();

            builder.RegisterControllers(typeof(MvcApplication).Assembly).PropertiesAutowired();
            
            builder.RegisterType<BingGeocodeService>().As<IGeocodeService>().PropertiesAutowired();
            
            builder.RegisterType<FacebookUserCreator>().As<IUserCreatorService>().PropertiesAutowired();
            builder.RegisterType<PlayerService>().As<IPlayerService>().PropertiesAutowired();
            builder.RegisterType<GamesService>().As<IGamesService>().PropertiesAutowired();
            
            builder.RegisterType<GameProxy>().PropertiesAutowired();
            builder.RegisterType<PlayerProxy>().PropertiesAutowired();

            builder.RegisterType<PlayerRepository>().PropertiesAutowired();
            builder.RegisterType<GameRepository>().PropertiesAutowired();


            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

            app.UseAutofacMiddleware(container);
            app.UseAutofacMvc();

            ConfigureAuth(app);
        }
    }
}
