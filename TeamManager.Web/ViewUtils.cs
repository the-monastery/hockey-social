﻿using TeamManager.Model;
using TeamManager.Web.Models;

namespace TeamManager.Web
{
    public static class ViewUtils
    {
        public static EditProfileViewModel CreateEditProfileViewModel(ApplicationUser user)
        {
            return new EditProfileViewModel
                   {
                       User = user,
                   };
        }
    }
}
