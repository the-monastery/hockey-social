﻿using System;
using NUnit.Framework;
using TeamManager.DataAccess;
using TeamManager.Model;
using TeamManager.Services;
using TeamManager.Services.Geocoding;
using TeamManager.Services.Requests;

namespace TeamManager.ServiceTests
{
    [TestFixture]
    public class BingGeocodeTests
    {
        private PlayerService _service;

        [SetUp]
        public void Setup()
        {
            _service = new PlayerService();
        }

        [TearDown]
        public void Teardown()
        {
            _service = null;
        }

        [Test]
        public void GetLongitudeLatitude()
        {
            BingGeocodeService service = new BingGeocodeService();
            TeamManagerContext context = new TeamManagerContext();
            Tuple<double,double> d = service.GetLatitudeLongitude("boulevard chevrier, brossard");
            Player player = _service.Get(context, new GetPlayerRequest { Id = 5 });
            player.FirstName = "John";

            _service.Update(context, new UpdatePlayerRequest{Player = player});
        }
    }
}
