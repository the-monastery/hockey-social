﻿using TeamManager.DataAccess;
using TeamManager.Model;
using TeamManager.Services;
using TeamManager.Services.Requests;

namespace TeamManager.Proxies
{
    public class PlayerProxy
    {
        public IPlayerService PlayerService { get; set; }

        public void UpdatePlayer(TeamManagerContext context, Player player)
        {
            PlayerService.Update(context,new UpdatePlayerRequest { Player = player });
        }
    }
}
