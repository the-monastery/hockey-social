﻿using System.Collections.Generic;
using TeamManager.DataAccess;
using TeamManager.Model;
using TeamManager.Services;
using TeamManager.Services.Requests;

namespace TeamManager.Proxies
{
    public class GameProxy
    {
        public IGamesService Service { get; set; }

        public List<Game> GetAll(TeamManagerContext context)
        {
            return Service.GetAll(context);
        }

        public Game GetById(TeamManagerContext context, int id)
        {
            return Service.Get(context, new GetGameRequest { Id = id });
        }

        public void AddGame(TeamManagerContext context, Game game)
        {
            Service.AddGame(context, new AddGameRequest { Game = game });
        }

        public void RemoveGame(TeamManagerContext context, int id)
        {
            Service.Delete(context, new DeleteGameRequest { Id = id });
        }

        public void UpdateGame(TeamManagerContext context, Game game)
        {
            Service.Update(context, new UpdateGameRequest { Game = game });
        }

        public void SaveGame(TeamManagerContext context, Game game)
        {
            Service.Update(context, new UpdateGameRequest { Game = game });
        }
    }
}
