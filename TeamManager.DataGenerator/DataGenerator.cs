﻿using System;
using System.Collections.Generic;
using System.Linq;
using TeamManager.Model;
using TeamManager.Model.enums;

namespace TeamManager.DataGenerator
{
    public static class DataGenerator
    {
        private static readonly Random Random = new Random();

        public static List<string> Colours = new List<string>
                                             {
                                                 "Red",
                                                 "Black",
                                                 "Blue",
                                                 "Green",
                                                 "Yellow",
                                                 "Pink",
                                                 "White",
                                             }; 

        public static Player CreatePlayer()
        {
            return new Player
            {
                Address = CreateAddress(),
                Contact = CreateContact(),
                Handedness = GetRandomEnumValue<Handedness>(),
                Gender = GetRandomEnumValue<Gender>(),
                HockeySkills = CreateList(CreateHockeySkill()),
                FirstName = Faker.Name.First(),
                LastName = Faker.Name.Last(),
                BirthDate = RandomDateTime(),
                CreationDate = RandomDateTime(),
                LastUpdate = RandomDateTime(),
            };
        }

        public static HockeySeason CreateHockeySeason()
        {
            return new HockeySeason
                       {
                           CreationDate = RandomDateTime(),
                           EndDate = RandomDateTime(),
                           LastUpdate = RandomDateTime(),
                           StartDate = RandomDateTime(),
                       };
        }

        public static League CreateLeague()
        {
            return new League
                       {
                           CreationDate = RandomDateTime(),
                           LastUpdate = RandomDateTime(),
                           LastSeason = CreateHockeySeason(),
                           PastSeasons = CreateList(CreateHockeySeason()),
                           Teams = CreateList(CreateTeam(),3),
                       };
        }

        public static Team CreateTeam()
        {
            return new Team
                       {
                           Color = Colours.ElementAt(Random.Next(0, Colours.Count)),
                           CurrentRoster = CreateRoster(),
                           CreationDate = RandomDateTime(),
                           LastUpdate = RandomDateTime(),
                       };
        }

        public static HockeySkill CreateHockeySkill()
        {
            return new HockeySkill
            {
                HighestCategoryPlayed = GetRandomEnumValue<Category>(),
                HighestLevelPlayed = GetRandomEnumValue<Level>(),
                Position = GetRandomEnumValue<Position>(),
                CreationDate = RandomDateTime(),
                LastUpdate = RandomDateTime(),
            };
        }

        public static Roster CreateRoster()
        {
            return new Roster
                       {
                           CreationDate = RandomDateTime(),
                           LastUpdate = RandomDateTime(),
                           Players = CreateList(CreatePlayer(), 10),
                       };
        }

        public static Organizer CreateOrganizer()
        {
            return new Organizer
            {
                Address = CreateAddress(),
                BirthDate = RandomDateTime(),
                Contact = CreateContact(),
                CreationDate = RandomDateTime(),
                LastUpdate = RandomDateTime(),
                FirstName = Faker.Name.First(),
                LastName = Faker.Name.Last(),
                Gender = GetRandomEnumValue<Gender>(),
                Handedness = GetRandomEnumValue<Handedness>(),
                Leagues = CreateList(CreateLeague()),
            };
        }


        public static Arena CreateArena()
        {
            return new Arena
                       {
                           Address = CreateAddress(),
                           CreationDate = RandomDateTime(),
                           LastUpdate = RandomDateTime(),
                       };
        }

        public static Game CreateGame()
        {
            //List<Team> tempTeamList = new List<Team>(Teams);
            //Team homeTeam = GetRandomValueFromList(tempTeamList);
            //tempTeamList.Remove(homeTeam);
            //Team visitingTeam = GetRandomValueFromList(tempTeamList);
            return new Game
                       {
                           CreationDate = RandomDateTime(),
                           DateTime = RandomDateTime(),
                           LastUpdate = RandomDateTime(),
                           HomeTeam = CreateTeam(),
                           VisitingTeam = CreateTeam(),
                           Arena = CreateArena(),
                           Duration = new TimeSpan(0, 1, 20),
                       };
        }

        public static Address CreateAddress()
        {
            return new Address
            {
                Code = Faker.Address.ZipCode(),
                Country = Faker.Address.Country(),
                Line1 = Faker.Address.StreetAddress(),
                Line2 = Faker.Address.SecondaryAddress(),
                CreationDate = RandomDateTime(),
                LastUpdate = RandomDateTime(),
                City = Faker.Address.City(),
            };
        }

        public static Contact CreateContact()
        {
            return new Contact
                       {
                           Email = Faker.Internet.Email(),
                           PhoneNumber1 = Faker.Phone.Number(),
                           PhoneNumber2 = Faker.Phone.Number(),
                           CreationDate = RandomDateTime(),
                           LastUpdate = RandomDateTime(),
                       };
        }

        public static DateTime RandomDateTime()
        {
            DateTime start = new DateTime(1995, 1, 1);
            Random gen = new Random();
            int range = (DateTime.Today - start).Days;

            return start.AddDays(gen.Next(range));
        }

        private static T GetRandomEnumValue<T>()
        {
            Random rand = new Random();
            T[] values = (T[])(Enum.GetValues(typeof(T)));

            return values[rand.Next(0, values.Length)];
        }

        private static List<T> GetSubList<T>(List<T> list)
        {
            int listSize = list.Count;
            int starting = Random.Next(0, listSize - 1);
            int ending = Random.Next(starting, listSize);

            return list.GetRange(starting, (ending - starting));
        }

        private static T GetRandomValueFromList<T>(List<T> list)
        {
            int listSize = list.Count;
            int index = Random.Next(0, listSize);

            return list[index];
        }

        private static List<T> CreateList<T>(T func, int count = 1)
        {
            List<T> output = new List<T>();

            for (int i = 0; i < count; i++)
            {
                output.Add(func);
            }

            return output;
        }
    }
}
